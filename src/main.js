import {Game} from "./Game.js";

/* main.js
 *
 * Copyright 2018 Remco Kranenburg <remco@burgsoft.nl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

function main() {
  const game = new Game();

  addEventListener('keydown', (e) => {
    if(e.key == "ArrowLeft") {
      game.player.movingLeft = true;
    } else if(e.key == "ArrowRight") {
      game.player.movingRight = true;
    } else if(e.key == "ArrowUp") {
      game.player.movingUp = true;
    } else if(e.key == "ArrowDown") {
      game.player.movingDown = true;
    }
  });

  addEventListener('keyup', (e) => {
    if(e.key == "ArrowLeft") {
      game.player.movingLeft = false;
    } else if(e.key == "ArrowRight") {
      game.player.movingRight = false;
    } else if(e.key == "ArrowUp") {
      game.player.movingUp = false;
    } else if(e.key == "ArrowDown") {
      game.player.movingDown = false;
    }
  });

  document.querySelector("#retry").addEventListener('click', () => game.start());

  game.start();
}

main();
