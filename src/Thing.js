/* Thing.js
 *
 * Copyright 2018 Remco Kranenburg <remco@burgsoft.nl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

export class Thing {
  constructor(position, radius) {
    this.position = position;
    this.radius = radius;
  }
  
  collidesWith(other) {
    const distance2 = this.position.distance2(other.position);
    const combinedRadius = this.radius + other.radius;
    const combinedRadius2 = combinedRadius * combinedRadius;

    return distance2 < combinedRadius2;
  }
}
