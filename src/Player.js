import {Actor} from "./Actor.js"
import {Point} from "./Point.js"

/* Player.js
 *
 * Copyright 2018 Remco Kranenburg <remco@burgsoft.nl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

export const PLAYER_RADIUS = 0.05;
export const PLAYER_SPEED = 0.005;
export const PLAYER_HEALTH = 100;

export class Player extends Actor {
  constructor() {
    super(new Point(0.5, 0.1), PLAYER_RADIUS, PLAYER_SPEED);
    this._health = PLAYER_HEALTH;
    this.score = 0;
    this.scoringInterval = setInterval(() => this.score++, 100);
  }

  get health() {
    return this._health;
  }

  set health(x) {
    this._health = Math.max(0, x);
    document.querySelector("#health").textContent = this._health;
  }

  get score() {
    return this._score;
  }

  set score(x) {
    this._score = x;
    document.querySelector("#score").textContent = this._score;
  }

  stopScoring() {
    clearInterval(this.scoringInterval);
  }

  reset() {
    this.position.x = 0.5;
    this.position.y = 0.1;
  }

  draw(context) {
    context.fillStyle = "white";
    context.beginPath();
    context.arc(this.position.x, this.position.y, this.radius, 0, Math.PI * 2);
    context.fill();
  }

  update() {
    super.update();
    this.confine();
  }

  confine() {
    this.position.x = Math.max(this.position.x, this.radius);
    this.position.x = Math.min(this.position.x, 1 - this.radius);
    this.position.y = Math.max(this.position.y, this.radius);
    this.position.y = Math.min(this.position.y, 1 - this.radius);
  }

  registerHit(damage) {
    this.health -= damage;
    return this.health > 0;
  }
}
