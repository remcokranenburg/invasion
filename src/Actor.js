import {Thing} from "./Thing.js"

/* Thing.js
 *
 * Copyright 2018 Remco Kranenburg <remco@burgsoft.nl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

export class Actor extends Thing {
  constructor(position, radius, speed) {
    super(position, radius);
    this.speed = speed;
    this.movingLeft = false;
    this.movingRight = false;
    this.movingUp = false;
    this.movingDown = false;
  }

  update() {
    if(this.movingLeft) {
      this.position.x -= this.speed;
    }

    if(this.movingRight) {
      this.position.x += this.speed;
    }

    if(this.movingUp) {
      this.position.y += this.speed;
    }

    if(this.movingDown) {
      this.position.y -= this.speed;
    }

  }
}
