import {Player, PLAYER_RADIUS} from "./Player.js"
import {Enemy, ENEMY_RADIUS} from "./Enemy.js"
import {Point} from "./Point.js"

/* Game.js
 *
 * Copyright 2018 Remco Kranenburg <remco@burgsoft.nl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

export class Game {
  constructor() {
    this.canvasElement = document.querySelector("canvas");
    this.gameOverElement = document.querySelector("#game-over");
    this.healthElement = document.querySelector("#health");
    this.context = this.canvasElement.getContext("2d");
  }

  start() {
    this.clearScreen();

    this.player = new Player();
    this.enemies = [];
    this.enemiesInterval = setInterval(() => {
      let enemy = new Enemy(new Point(Math.random(), 1.0 + ENEMY_RADIUS));
      this.enemies.push(enemy);
      console.log("enemy spawned");
    }, 2000);

    this.gameOverElement.style.display = 'none';
    this.canvasElement.style.display = 'block';
    this.healthElement.textContent = this.player.health;

    requestAnimationFrame(() => this.draw());
  }

  handleGameOver() {
    console.log("player died");
    this.gameOverElement.style.display = 'block';
    this.canvasElement.style.display = 'none';
    clearInterval(this.enemiesInterval);
    this.player.stopScoring();
  }

  draw() {
    this.resizeCanvasToBody();
    this.clearScreen();

    this.context.setTransform(
        this.canvasElement.width,
        0,
        0,
        -this.canvasElement.height,
        0,
        this.canvasElement.height);

    this.player.update();
    this.player.draw(this.context);

    let dead = false;

    this.enemies.some((enemy, i) => {
      enemy.update(this.player);
      enemy.draw(this.context);

      if(this.player.collidesWith(enemy)) {
        console.log("enemy died");
        if(this.player.registerHit(enemy.damage)) {
          this.enemies.splice(i, 1);
        } else {
          this.handleGameOver();
          dead = true;
        }
        return true;
      }
    });

    this.enemies = this.enemies.filter((enemy1, i1) => {
      const isHit = this.enemies.some((enemy2, i2) => i1 != i2 && enemy1.collidesWith(enemy2));

      if(isHit) {
        console.log("enemy died");
      }

      return !isHit;
    });

    if(!dead) {
      requestAnimationFrame(() => this.draw());
    }
  }

  resizeCanvasToBody() {
    const size = Math.min(document.body.offsetWidth, document.body.offsetHeight);
    this.canvasElement.width = this.canvasElement.height = size;
    this.canvasElement.style.width = this.canvasElement.style.height = size + 'px';
  }

  clearScreen() {
    this.context.fillStyle = "black";
    this.context.fillRect(0, 0, this.canvasElement.width, this.canvasElement.height);
  }
}
