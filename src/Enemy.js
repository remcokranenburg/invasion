import {Actor} from "./Actor.js"

/* Enemy.js
 *
 * Copyright 2018 Remco Kranenburg <remco@burgsoft.nl>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

export const ENEMY_RADIUS = 0.03;
export const ENEMY_SPEED = 0.002;
export const ENEMY_DAMAGE = 40;

export class Enemy extends Actor {
  constructor(position) {
    super(position, ENEMY_RADIUS, ENEMY_SPEED);
    this.damage = ENEMY_DAMAGE;
  }

  draw(context) {
    context.fillStyle = "red";
    context.beginPath();
    context.arc(this.position.x, this.position.y, this.radius, 0, Math.PI * 2);
    context.fill();
  }

  update(player) {
    if(this.position.x < player.position.x - player.radius + this.radius) {
      this.movingRight = true;
      this.movingLeft = false;
    } else if(this.position.x > player.position.x + player.radius - this.radius) {
      this.movingRight = false;
      this.movingLeft = true;
    } else {
      this.movingRight = false;
      this.movingLeft = false;
    }

    if(this.position.y < player.position.y - player.radius + this.radius) {
      this.movingUp = true;
      this.movingDown = false;
    } else if(this.position.y > player.position.y + player.radius - this.radius) {
      this.movingUp = false;
      this.movingDown = true;
    } else {
      this.movingUp = false;
      this.movingDown = false;
    }

    super.update();
  }
}
